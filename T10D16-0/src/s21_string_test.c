#include "s21_string.h"

#include <stdio.h>
#include <stdlib.h>

void s21_strlen_test(char str[], size_t lenght);
void s21_strcmp_test(char string1[], char string2[], int result);
void s21_strcpy_test(char string1[]);
void s21_strcat_test(char string1[], char string2[], char result_string[]);
void s21_strchr_test(char string[], int num, int result);
void s21_strstr_test(char string1[], char string2[], char result_string[]);
void s21_strtok_test(char string1[], char string2[]);

int main() {
    s21_strlen_test("Hello!", 6);
    s21_strcmp_test("Hello", "Hello", 0);
    s21_strcpy_test("Hello");
    s21_strcat_test("Hello ", "World", "Hello World");
    s21_strchr_test("Hello ", 'H', 0);
    return 0;
}
void s21_strlen_test(char str[], size_t lenght) {
    printf("%s ", str);
    printf("%ld ", s21_strlen(str));
    if (s21_strlen(str) == lenght) {
        printf("SUCCESS\n");
    } else {
        printf("FAIL\n");
    }
}
void s21_strcmp_test(char string1[], char string2[], int result) {
    printf("%s ", string1);
    printf("%s ", string2);
    printf("%d ", s21_strcmp(string1, string2));
    if (s21_strcmp(string1, string2) == result) {
        printf("SUCCESS\n");
    } else {
        printf("FAIL\n");
    }
}
void s21_strcpy_test(char string1[]) {
    char string2[s21_strlen(string1)];
    printf("%s ", string1);
    printf("%s ", s21_strcpy(string2, string1));
    if (s21_strcmp(string1, string2) == 0) {
        printf("SUCCESS\n");
    } else {
        printf("FAIL\n");
    }
}
void s21_strcat_test(char string1[], char string2[], char result_string[]) {
    char* string3 = (char*)calloc((s21_strlen(string1) + s21_strlen(string2)), sizeof(char));
    printf("%s %s ", string1, string2);
    printf("%s ", s21_strcat(s21_strcat(string3, string1), string2));
    if (s21_strcmp(string3, result_string) == 0) {
        printf("SUCCESS\n");
    } else {
        printf("FAIL\n");
    }
    if (string3 != NULL) free(string3);
}
void s21_strchr_test(char string[], int num, int result) {
    printf("%s %d ", string, num);
    printf("%s ", s21_strchr(string, num));
    if (string[result] == *s21_strchr(string, num)) {
        printf("SUCCESS\n");
    } else {
        printf("FAIL\n");
    }
}
void s21_strstr_test(char string1[], char string2[], char result_string[]) {
    printf("%s %s %s ", string1, string2, s21_strstr("Hello World Ai", "World"));
    if (*result_string == *s21_strstr("Hello World Ai", "World"))
        printf("SUCCESS\n");
    else {
        printf("FAIL\n");
    }
}
void s21_strtok_test(char string1[], char string2[]) {
    printf("%s %s %s ", string1, string2, s21_strtok("Hello! World", "!"));
}
