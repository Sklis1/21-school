#include "s21_string.h"

size_t s21_strlen(const char *str) {
    size_t length = 0;
    for (; *(str + length); length++)
        ;
    return length;
}
int s21_strcmp(const char *str1, const char *str2) {
    for (; *str1 && *str1 == *str2; str1++, str2++)
        ;
    return *str1 - *str2;
}
char *s21_strcpy(char *str2, const char *str1) {
    char *ptr = str2;
    while (*str1 != '\0') {
        *str2 = *str1;
        str1++;
        str2++;
    }
    *str2 = '\0';
    return ptr;
}
char *s21_strcat(char *str2, const char *str1) {
    char *ptr = str2 + s21_strlen(str2);
    while (*str1 != 0) {
        *ptr++ = *str1++;
    }
    *ptr = '\0';
    return str2;
};
char *s21_strchr(const char *str, int num) {
    int i = 0;
    while (str[i] && str[i] != num) i++;
    return num == str[i] ? (char *)str + i : NULL;
}
char *s21_strstr(char *string, char *substring) {
    char *a, *b;
    b = substring;
    if (*b == 0) {
        return string;
    }
    for (; *string != 0; string += 1) {
        if (*string != *b) {
            continue;
        }
        a = string;
        while (1) {
            if (*b == 0) {
                return string;
            }
            if (*a++ != *b++) {
                break;
            }
        }
        b = substring;
    }
    return NULL;
}
unsigned int is_delim(char c, char *delim) {
    while (*delim != '\0') {
        if (c == *delim) return 1;
        delim++;
    }
    return 0;
}
char *s21_strtok(char *srcString, char *delim) {
    static char *backup_string;  // start of the next search
    if (!srcString) {
        srcString = backup_string;
    }
    if (!srcString) {
        // user is bad user
        return NULL;
    }
    // handle beginning of the string containing delims
    while (1) {
        if (is_delim(*srcString, delim)) {
            srcString++;
            continue;
        }
        if (*srcString == '\0') {
            // we've reached the end of the string
            return NULL;
        }
        break;
    }
    char *ret = srcString;
    while (1) {
        if (*srcString == '\0') {
            /*end of the input string and
            next exec will return NULL*/
            backup_string = srcString;
            return ret;
        }
        if (is_delim(*srcString, delim)) {
            *srcString = '\0';
            backup_string = srcString + 1;
            return ret;
        }
        srcString++;
    }
}
