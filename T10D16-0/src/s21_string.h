#include <stdio.h>
#include <stdlib.h>
size_t s21_strlen(const char* str);
int s21_strcmp(const char* str1, const char* str2);
char* s21_strcpy(char* str2, const char* str1);
char* s21_strcat(char* str2, const char* str1);
char* s21_strchr(const char* str, int ch);
char* s21_strstr(char* string, char* substring);
unsigned int is_delim(char c, char* delim);
char* s21_strtok(char* srcString, char* delim);
