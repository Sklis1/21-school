#include <stdio.h>

int div(int n, int d);
int res(int n, int d);

int main() {
    int input, max = 2;
    char c;

    if ((scanf("%d%c", &input, &c) == 2) && (c == '\n')) {
        if (input == 0) printf("%d", 0);
        if (input == 1) printf("%d", 1);
        if (input == -1) printf("%d", -1);
        if (input < 0) input = input * -1;

        max = div(input, max);
        printf("%d", max);
    }

    else {
        printf("n/a");
        return 1;
    }
    return 0;
}

int div(int input, int max) {
    for (int i = 2; i <= input; i++) {
        int k = 0;
        for (int j = 2; j <= i; j++) {
            if (res(i, j) == 0) {
                k++;
            }
        }

        if (k == 1) {
            if (res(input, i) == 0) {
                if (i > max) {
                    max = i;
                }
            }
        }
    }
    return max;
}

int res(int num, int dev) {
    for (int i = 0; num >= dev; i++) {
        num = num - dev;
    }
    return num;
}
