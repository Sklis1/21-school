#include <stdio.h>

int decoder(int *flag, int *N);
int coder(int *flag, int *N);

int main() {
    int x, flag = 0, N = 100;

    if (scanf("%d", &x) == 1 && (x == 0 || x == 1)) {
        if (x == 1) decoder(&flag, &N);
        if (x == 0) coder(&flag, &N);
    } else {
        printf("n/a");
    }

    if (flag == 1) printf("n/a");

    return 0;
}

int decoder(int *flag, int *N) {
    int strLength = 0, arrInt[*N];
    char str;

    for (int i = 0; i < *N; i++) {
        if (scanf("%X %c", &arrInt[i], &str) == 2 && (str == '\n' || str == ' ')) {
            if (str == ' ') {
                strLength++;
                continue;
            } else if (str == '\n') {
                break;
            } else {
                *flag = 1;
            }
        } else {
            *flag = 1;
            break;
        }
    }

    if (flag == 0) {
        for (int i = 0; i <= strLength; i++) {
            if (i == strLength) printf("%c", arrInt[i]);
            printf("%c ", arrInt[i]);
        }
    }

    return *flag;
}

int coder(int *flag, int *N) {
    char str[2];

    do {
        scanf("%s", &str);
        if (str[1] != ' ' || str[1] != '\n') {
            *flag = 1;
            break;
        }

        if (str[1] == '\n') {
            printf("%X", str[0]);
            break;
        }
        printf("%X ", str[0]);

    } while (1);

    return *flag;
}