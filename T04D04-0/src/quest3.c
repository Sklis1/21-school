#include <math.h>
#include <stdio.h>

int fibo(int n);

int main() {
    int num, res;
    char c;
    res = scanf("%d%c", &num, &c);
    if (res == 2 && c == '\n' && num > 0) {
        res = fibo(num);
        printf("%d", res);
    } else {
        printf("n/a");
    }
    return 0;
}

int fibo(int n) {
    if (n == 1 || n == 2) {
        return 1;
    } else {
        return fibo(n - 1) + fibo(n - 2);
    }
}