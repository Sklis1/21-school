#include <math.h>
#include <stdio.h>

double funAnezi(double num);
double funBernulli(double num);
double funGiperbola(double num);

int main() {
    double step = M_PI / 20.5;
    for (double i = -M_PI; i <= M_PI; i += step) {
        double fist = funAnezi(i);
        double second = funBernulli(i);
        double third = funGiperbola(i);
        
    }
    return 0;
}

double funAnezi(double num) { return 1 / (1 + pow(num, 2)); }

double funBernulli(double num) { return sqrt(sqrt(1 + 4 * pow(num, 2) * 1) - pow(num, 2) - 1); }

double funGiperbola(double num) { return 1 / pow(num, 2); }