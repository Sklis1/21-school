#include <math.h>
#include <stdio.h>

double anezi(double num);
double bernulli(double num);
double giperbola(double num);

int main() {
    double step = M_PI / 20.5;
    for (double i = -M_PI; i <= M_PI; i = i + step) {
        double fist = anezi(i);
        double second = bernulli(i);
        double three = giperbola(i);

        if (isnan(fist)) {
            printf("%.7lf | - | %.7lf | %.7lf\n", i, second, three);
        }
        if (isnan(second)) {
            printf("%.7lf | %.7lf | - | %.7lf\n", i, fist, three);
        }
        if (isnan(three)) {
            printf("%.7lf | %.7lf | %.7lf | -\n", i, fist, second);
        }
        if (!isnan(fist) && !isnan(second) && !isnan(three)) {
            printf("%.7lf | %.7lf | %.7lf | %.7lf\n", i, fist, second, three);
        }
    }
    return 0;
}

double anezi(double num) {
    double res = 1 / (1 + pow(num, 2));
    return res;
}

double bernulli(double num) { return sqrt(sqrt(pow(1,4) + 4 * pow(num, 2) * pow(1,2)) - pow(num, 2) - pow(1,2)); }

double giperbola(double num) { return (1 / pow(num, 2)); }