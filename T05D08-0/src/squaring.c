#include <stdio.h>
#define NMAX 10

int input(int *arr, int *n);
void output(int *arr, int n);
void squaring(int *arr, int n);

int main() {
    int n, data[NMAX];

    if (input(data, &n) == 0) {
        squaring(data, n);
        output(data, n);
    } else {
        printf("n/a");
    }
    return 0;
}

int input(int *arr, int *n) {
    int flagMistake = 0;
    scanf("%d", n);
    if (*n <= NMAX) {
        for (int i = 0; i < *n; i++) {
            char c;
            int num;
            if (scanf("%d%c", &num, &c) == 2 && (c == '\n' || c == ' ')) {
                arr[i] = num;
                continue;
            } else {
                flagMistake = 1;
            }
        }
    } else {
        flagMistake = 1;
    }
    return flagMistake;
}

void output(int *arr, int n) {
    for (int i = 0; i < n; i++) {
        if (i == n - 1) {
            printf("%d", arr[i]);
        } else if (i == 0) {
            printf("\n%d", arr[i]);
        } else {
            printf("%d ", arr[i]);
        }
    }
}

void squaring(int *arr, int n) {
    for (int i = 0; i < n; i++) {
        arr[i] *= arr[i];
    }
}
