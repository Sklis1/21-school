#include <limits.h>
#include <math.h>
#include <stdio.h>

#define NMAX 10

int input(int *arr, int *n);
void output(int *arr, int n);
int max(int *arr, int n);
int min(int *arr, int n);
double mean(int *arr, int n);
double variance(int *arr, int n, double m);

void output_result(int max_v, int min_v, double mean_v, double variance_v);

int main() {
    int n, data[NMAX];
    if (input(data, &n) == 0) {
        output(data, n);
        double m = mean(data, n);
        output_result(max(data, n), min(data, n), mean(data, n), variance(data, n, m));
    }

    return 0;
}

int input(int *arr, int *n) {
    int flagMistake = 0;
    scanf("%d", n);
    if (*n <= NMAX) {
        for (int i = 0; i < *n; i++) {
            char c;
            int num;
            if (scanf("%d%c", &num, &c) == 2 && (c == '\n' || c == ' ')) {
                arr[i] = num;
                continue;
            } else {
                flagMistake = 1;
            }
        }
    } else {
        flagMistake = 1;
    }
    return flagMistake;
}

int max(int *arr, int n) {
    int res = INT_MIN;
    for (int i = 0; i < n; i++) {
        if (arr[i] > res) res = arr[i];
    }
    return res;
}

int min(int *arr, int n) {
    int res = INT_MAX;
    for (int i = 0; i < n; i++) {
        if (arr[i] < res) res = arr[i];
    }
    return res;
}

double mean(int *arr, int n) {
    double res = 0;
    for (int i = 0; i < n; i++) {
        res += arr[i];
    }
    return res /= n;
}

double variance(int *arr, int n, double m) {
    double res = 0;
    for (int i = 0; i < n; i++) {
        res += pow((arr[i] - m), 2);
    }
    res /= n;
    return res;
}

void output(int *arr, int n) {
    for (int i = 0; i < n; i++) {
        if (i == n - 1) {
            printf("%d", arr[i]);
        } else if (i == 0) {
            printf("\n%d", arr[i]);
        } else {
            printf("%d ", arr[i]);
        }
    }
}

void output_result(int max_v, int min_v, double mean_v, double variance_v) {
    printf("\n%d %d %.6lf %.6lf", max_v, min_v, mean_v, variance_v);
}
