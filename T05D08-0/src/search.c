#include <limits.h>
#include <math.h>
#include <stdio.h>
/*
    Search module for the desired value from data array.

    Returned value must be:
        - "even"+
        - ">= mean"
        - "<= mean + 3 * sqrt(variance)"
        - "!= 0"

        OR

        0
*/

int input(int *arr, int *n);
double mean(int *arr, int n);
int res(int *arr, int n);
double variance(int *arr, int n, double m);

int main() {
    int n, arr[30];

    // variance(data, n, m);
    if (input(arr, &n) == 0) {
        if (res(arr, n) != 1) printf("0");

    } else {
        printf("n/a");
    }
    return 0;
}

int input(int *arr, int *n) {
    int flagMistake = 0;
    scanf("%d", n);
    if (*n <= 30) {
        for (int i = 0; i < *n; i++) {
            char c;
            int num;
            if (scanf("%d%c", &num, &c) == 2 && (c == '\n' || c == ' ')) {
                arr[i] = num;
                continue;
            } else {
                flagMistake = 1;
            }
        }
    } else {
        flagMistake = 1;
    }
    return flagMistake;
}

double mean(int *arr, int n) {
    double res = 0;
    for (int i = 0; i < n; i++) {
        res += arr[i];
    }
    return res /= n;
}

int res(int *arr, int n) {
    int flag = 0;
    for (int i = 0; i < n; i++) {
        if (arr[i] % 2 == 0 && arr[i] >= mean(arr, n) &&
            arr[i] <= (mean(arr, n) + 3 * sqrt(variance(arr, n, mean(arr, n)))) && arr[i] != 0) {
            printf("%d", arr[i]);
            flag = 1;
        }
    }
    return flag;
}

double variance(int *arr, int n, double m) {
    double res = 0;
    for (int i = 0; i < n; i++) {
        res += pow((arr[i] - m), 2);
    }
    res /= n;
    return res;
}