#include <stdio.h>
#include <stdlib.h>

int input(int *arr, const int n);
void sort(int *arr, int n);
void print(int *arr, int n);

int main() {
    int n;
    char l;
    if (scanf("%d%c", &n, &l) != 2 && l != '\n') {
        printf("n/a");
    } else {
        int *arr = malloc(n * sizeof(int));
        if (arr != NULL) {
            if (input(arr, n) == 0) {
                sort(arr, n);
                print(arr, n);
                free(arr);
            } else {
                printf("n/a");
            }
        }
    }
    return 0;
}

int input(int *arr, const int n) {
    int flagMistake = 0;
    for (int i = 0; i < n; i++) {
        char c;
        if (scanf("%d%c", &arr[i], &c) == 2 && (c == '\n' || c == ' ')) {
            continue;
        } else {
            flagMistake = 1;
        }
    }
    return flagMistake;
}

void sort(int *arr, int n) {
    for (int i = 0; i < n; i++) {
        int element;
        int index;
        element = arr[i];
        index = i - 1;
        while (index >= 0 && arr[index] > element) {
            arr[index + 1] = arr[index];
            index--;
        }
        arr[index + 1] = element;
    }
}

void print(int *arr, int n) {
    for (int i = 0; i < n; i++) {
        if (i == n - 1) {
            printf("%d", arr[i]);
        } else if (i == 0) {
            printf("\n%d ", arr[i]);
        } else {
            printf("%d ", arr[i]);
        }
    }
}
