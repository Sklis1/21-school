#include "data_io.h"

#include <stdio.h>

int input(double *data, int n) {
    int flagMistake = 0;
    char c;

    for (int i = 0; i < n; i++) {
        if (scanf("%lf%c", &data[i], &c) == 2 && (c == '\n' || c == ' ')) {
            continue;
        } else {
            flagMistake = 1;
        }
    }

    return flagMistake;
}

void output(double *data, int n) {
    for (int i = 0; i < n; i++) {
        printf("%.2lf ", data[i]);
    }
}