#include "data_stat.h"

#include <math.h>

double max(double *data, int n) {
    double x = data[0];
    for (int i = 0; i < n; i++) {
        if (data[i] > x) x = data[i];
    }
    return x;
}

double min(double *data, int n) {
    double x = data[0];
    for (int i = 0; i < n; i++) {
        if (data[i] < x) x = data[i];
    }
    return x;
}

double mean(double *data, int n) {
    double x = 0;
    for (int i = 0; i < n; i++) {
        x += data[i];
    }
    return x / n;
}

double variance(double *data, int n) {
    double res = 0;
    for (int i = 0; i < n; i++) {
        res += pow((data[i] - mean(data, n)), 2);
    }
    return res /= n;
}

void sort(double *data, int n) {
    for (int i = 0; i < n; i++) {
        double element;
        int index;
        element = data[i];
        index = i - 1;
        while (index >= 0 && data[index] > element) {
            data[index + 1] = data[index];
            index--;
        }
        data[index + 1] = element;
    }
}