
#include <stdio.h>
#include <stdlib.h>

#include "../data_libs/data_io.h"
#include "../data_libs/data_stat.h"
#include "../data_module/data_process.h"

int main() {
    int n;
    char c;

    if (scanf("%d%c", &n, &c) == 2 && c == '\n') {
        if (n <= 0) {
            printf("ERROR");
        } else {
            double *data = malloc(n * sizeof(double));
            if (data != NULL) {
                if (input(data, n) == 0) {
                    if (normalization(data, n) == 1) {
                        output(data, n);
                    } else {
                        printf("ERROR");
                    }
                } else {
                    printf("ERROR");
                }
            } else {
                printf("ERROR");
            }
            free(data);
        }
    } else {
        printf("ERROR");
    }

    return 0;
}
