#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include "door_struct.h"

#define door_struct

#define DOORS_COUNT 15
#define MAX_ID_SEED 10000

void initialize_doors(struct door* doors);
void sort(struct door* doors);
void output(struct door* doors);
void chenge_status0(struct door* doors);
void doors_struct();

int main() {
#ifdef door_struct
    doors_struct();
#endif

    return 0;
}

// Doors initialization function
// ATTENTION!!!
// DO NOT CHANGE!
void initialize_doors(struct door* doors) {
    srand(time(0));

    int seed = rand() % MAX_ID_SEED;
    for (int i = 0; i < DOORS_COUNT; i++) {
        doors[i].id = (i + seed) % DOORS_COUNT;
        doors[i].status = rand() % 2;
    }
}

void doors_struct() {
    struct door doors[DOORS_COUNT];
    initialize_doors(doors);
    sort(doors);
    chenge_status0(doors);
    output(doors);
}

void sort(struct door* doors) {
    for (int i = 0; i < DOORS_COUNT - 1; i++) {
        // сравниваем два соседних элемента.
        for (int j = 0; j < DOORS_COUNT - i - 1; j++) {
            if (doors[j].id > doors[j + 1].id) {
                // если они идут в неправильном порядке, то
                //  меняем их местами.
                int tmp = doors[j].id;
                doors[j].id = doors[j + 1].id;
                doors[j + 1].id = tmp;
            }
        }
    }
}

void chenge_status0(struct door* doors) {
    for (int i = 0; i < DOORS_COUNT; i++) {
        doors[i].status = 0;
    }
}

void output(struct door* doors) {
    for (int i = 0; i < DOORS_COUNT; i++) {
        printf("%d, %d\n", doors[i].id, doors[i].status);
    }
}
