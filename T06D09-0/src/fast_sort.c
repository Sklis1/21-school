#include <stdio.h>

int input(int *arr, int n);
void print(int *arr, int n);
void sort(int *arr, int n);
void heapSort(int *arr, int n);
void siftDown(int *arr, int root, int bottom);

int main() {
    int n = 10;
    int arr[10];

    if (input(arr, n) == 0) {
        sort(arr, &n);
        heapSort(arr, &n);
    } else {
        printf("n/a");
    }
    return 0;
}

int input(int *arr, int n) {
    int flagMistake = 0;

    for (int i = 0; i < n; i++) {
        char c;
        int num;
        if (scanf("%d%c", &num, &c) == 2 && (c == '\n' || c == ' ')) {
            arr[i] = num;
            continue;
        } else {
            flagMistake = 1;
        }
    }

    return flagMistake;
}

void sort(int *arr, int n) {
    for (int i = 0; i < n; i++) {
        int element, index;
        element = arr[i];
        index = i - 1;
        while (index >= 0 && arr[index] > element) {
            arr[index + 1] = arr[index];
            index--;
        }
        arr[index + 1] = element;
    }
    print(arr, n);
}

void heapSort(int *arr, int n) {
    // Формируем нижний ряд пирамиды
    for (int i = (n / 2); i >= 0; i--) siftDown(arr, i, n - 1);
    // Просеиваем через пирамиду остальные элементы
    for (int i = n - 1; i >= 1; i--) {
        int temp = arr[0];
        arr[0] = arr[i];
        arr[i] = temp;
        siftDown(arr, 0, i - 1);
    }
    print(arr, n);
}

void siftDown(int *arr, int root, int bottom) {
    int maxChild;  // индекс максимального потомка
    int done = 0;  // флаг того, что куча сформирована
    // Пока не дошли до последнего ряда
    while ((root * 2 <= bottom) && (!done)) {
        if (root * 2 == bottom)   // если мы в последнем ряду,
            maxChild = root * 2;  // запоминаем левый потомок
        // иначе запоминаем больший потомок из двух
        else if (arr[root * 2] > arr[root * 2 + 1])
            maxChild = root * 2;
        else
            maxChild = root * 2 + 1;
        // если элемент вершины меньше максимального потомка
        if (arr[root] < arr[maxChild]) {
            int temp = arr[root];  // меняем их местами
            arr[root] = arr[maxChild];
            arr[maxChild] = temp;
            root = maxChild;
        } else         // иначе
            done = 1;  // пирамида сформирована
    }
}

void print(int *arr, int n) {
    for (int i = 0; i < n; i++) {
        if (i == n - 1) {
            printf("%d", arr[i]);
        } else if (i == 0) {
            printf("\n%d ", arr[i]);
        } else {
            printf("%d ", arr[i]);
        }
    }
}