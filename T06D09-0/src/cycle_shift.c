#include <stdio.h>

#define NMAX 10

int input(int *arr, int *m, int *c);
void left(int *arr, int m, int c);
void right(int *arr, int m, int c);
void output(int *arr, int m);

int main() {
    int m, c;
    int arr[NMAX];
    if (input(arr, &m, &c) == 0) {
        if (c > 0) {
            left(arr, m, c);
        } else if (c < 0) {
            right(arr, m, c);
        } else {
            printf("n/a");
        }
    }
    return 0;
}

int input(int *arr, int *m, int *c) {
    int flagMistake = 0;
    char s, ss;
    scanf("%d", m);
    if (*m <= NMAX) {
        for (int i = 0; i < *m; i++) {
            int num;
            if (scanf("%d%c", &num, &ss) == 2 && (ss == '\n' || ss == ' ')) {
                arr[i] = num;
                continue;
            } else {
                flagMistake = 1;
            }
        }
    } else {
        flagMistake = 1;
    }
    if (scanf("%d%c", c, &s) != 2 && s != '\n') flagMistake = 1;
    return flagMistake;
}

void left(int *arr, int m, int c) {
    for (int i = 0; i < c; i++) {
        int tmp = arr[0];
        for (int j = 0; j < m - 1; j++) {
            arr[j] = arr[j + 1];
        }
        arr[m - 1] = tmp;
    }
    output(arr, m);
}

void right(int *arr, int m, int c) {
    for (int i = 0; i < c; i++) {
        int tmp = arr[m - 1];
        for (int j = m - 1; j > 0; j--) {
            arr[j] = arr[j - 1];
        }
        arr[0] = tmp;
    }
    output(arr, m);
}

void output(int *arr, int m) {
    for (int i = 0; i < m; i++) {
        printf("%d ", arr[i]);
    }
}