#include <stdio.h>

#define LEN 100

void sum(const int *buff1, const int *buff2, int *result, int n1, int n2, int nn);
void sub(const int *arr1, int n1, const int *arr2, int n2, int *result, int nn);
int input(int *arr1, int *arr2, int *n1, int *n2, int *nn);
void output(int *new_arr, int nn);

#define NMAX 100
/*
    Беззнаковая целочисленная длинная арифметика
    с использованием массивов.
    Ввод:
     * 2 длинных числа в виде массивов до 100 элементов
     * В один элемент массива нельзя вводить число > 9
    Вывод:
     * Результат сложения и разности чисел-массивов
    Пример:
     * 1 9 4 4 6 7 4 4 0 7 3 7 0 9 5 5 1 6 1
       2 9

       1 9 4 4 6 7 4 4 0 7 3 7 0 9 5 5 1 9 0
       1 9 4 4 6 7 4 4 0 7 3 7 0 9 5 5 1 3 2
*/
int main() {
    int n1, n2, nn;
    int arr1[NMAX], arr2[NMAX];
    if (input(arr1, arr2, &n1, &n2, &nn) == 0) {
        int result[nn];
        if (arr1[0] == 0 && n1 == 1 && arr2[0] > 0) {
            printf("n/a");
        } else if (arr1[0] > 0 && n1 == 1 && arr2[0] == 0) {
            printf("%d", arr1[0]);
        } else if (n2 > n1) {
            sum(arr1, arr2, result, n1, n2, nn);
            printf("\nn/a");
        } else if (arr1[0] < arr2[0] && n1 == n2) {
            sum(arr1, arr2, result, n1, n2, nn);
            printf("\nn/a");
        } else {
            sum(arr1, arr2, result, n1, n2, nn);
            sub(arr1, n1, arr2, n2, result, nn);
        }
    } else {
        printf("n/a");
    }
    return 0;
}

int input(int *arr1, int *arr2, int *n1, int *n2, int *nn) {
    int flagMistake = 0;
    *n1 = 0;
    *n2 = 0;
    for (int i = 0; i < NMAX; i++) {
        char c;
        if (scanf("%d%c", &arr1[i], &c) == 2 && ((c == '\n') || (c == ' ')) &&
            ((arr1[i] >= 0) && (arr1[i] <= 9))) {
            *n1 += 1;
        } else {
            flagMistake = 1;
        }
        if (c == '\n') break;
    }

    for (int i = 0; i < NMAX; i++) {
        char c;
        if (scanf("%d%c", &arr2[i], &c) == 2 && (c == '\n' || c == ' ') && (arr2[i] >= 0 && arr2[i] <= 9)) {
            *n2 += 1;
        } else {
            flagMistake = 1;
        }
        if (c == '\n') break;
    }
    *nn = *n1 + 1;

    return flagMistake;
}

void sum(const int *arr1, const int *arr2, int *result, int n1, int n2, int nn) {
    int r = nn - n2;
    result[0] = 0;
    for (int i = 0; i < n1; i++) {
        result[i + 1] = arr1[i];
    }

    for (int i = nn - 1; i > r - 1; i--) {
        if (result[i] + arr2[i - r] > 9) {
            result[i - 1] += 1;
            result[i] = (result[i] + arr2[i - r]) % 10;
        } else {
            result[i] += arr2[i - r];
        }
    }
    output(result, nn);
}

void sub(const int *arr1, int n1, const int *arr2, int n2, int *result, int nn) {
    int r = nn - n2;
    result[0] = 0;
    for (int i = 0; i < n1; i++) {
        result[i + 1] = arr1[i];
    }
    for (int i = nn - 1; i > r - 1; i--) {
        if (result[i] - arr2[i - r] < 0) {
            result[i - 1] -= 1;
            result[i] = (result[i] - arr2[i - r]) + 10;
        } else {
            result[i] -= arr2[i - r];
        }
    }
    printf("\n");
    output(result, nn);
}

void output(int *result, int nn) {
    int zero = 0;
    for (int i = 0; i < nn; i++) {
        if (result[i] == 0) {
            zero++;
        } else {
            break;
        }
    }
    if (zero != nn) {
        for (int i = 0 + zero; i < nn; i++) {
            printf("%d ", result[i]);
        }
    } else {
        printf("0");
    }
}
