/*------------------------------------
        Здравствуй, человек!
        Чтобы получить ключ
        поработай с комментариями.
-------------------------------------*/

#include <stdio.h>

#define NMAX 10

int input(int *arr, int *m);
void output(int *new_arr, int mm, int sum);
int sum_numbers(const int *arr, int m);
void find_numbers(const int *arr, int sum, int m);

/*------------------------------------
        Функция получает массив данных
        через stdin.
        Выдает в stdout особую сумму
        и сформированный массив
        специальных элементов
        (выбранных с помощью найденной суммы):
        это и будет частью ключа
-------------------------------------*/
int main() {
    int m;
    int arr[NMAX];
    if (input(arr, &m) == 0) {
        int sum = sum_numbers(arr, m);
        find_numbers(arr, sum, m);
    } else {
        printf("n/a");
    }
    return 0;
}

// Функция считывание и заполенние массива
int input(int *arr, int *m) {
    int flagMistake = 0;
    scanf("%d", m);
    if (*m <= NMAX) {
        for (int i = 0; i < *m; i++) {
            char c;
            int num;
            if (scanf("%d%c", &num, &c) == 2 && (c == '\n' || c == ' ')) {
                arr[i] = num;
                continue;
            } else {
                flagMistake = 1;
            }
        }
    } else {
        flagMistake = 1;
    }
    return flagMistake;
}

/*------------------------------------
        Функция должна находить
        сумму четных элементов
        с 0-й позиции.
-------------------------------------*/
int sum_numbers(const int *arr, int m) {
    int sum = 0;

    for (int i = 0; i < m; i++) {
        if (arr[i] != 0) {
            if (arr[i] % 2 == 0) sum += arr[i];
        }
    }
    return sum;
}

/*------------------------------------
        Функция должна находить
        все элементы, на которые нацело
        делится переданное число и
        записывает их в выходной массив.
-------------------------------------*/
void find_numbers(const int *arr, int sum, int m) {
    int mm = 0;
    for (int i = 0; i < m; i++) {
        if (arr[i] != 0) {
            if (sum % arr[i] == 0) {
                mm++;
            }
        }
    }
    int new_arr[mm];
    int x = 0;
    for (int i = 0; i < m; i++) {
        if (arr[i] != 0) {
            if (sum % arr[i] == 0) {
                new_arr[x] = arr[i];
                x++;
            }
        }
    }
    output(new_arr, mm, sum);
}

void output(int *new_arr, int mm, int sum) {
    printf("%d\n", sum);
    for (int i = 0; i < mm; i++) {
        printf("%d ", new_arr[i]);
    }
}