#include <stdio.h>
#include <stdlib.h>
#define NMAX 100

void dynamics1(int line, int column);
void dynamics2(int line, int column);
void dynamics3(int line, int column);
void stati(int line, int column);
void output(int arr[][NMAX], int line, int column);
void output_d(int **arr, int line, int column);

int main() {
    int n, line, column;
    char l, c;
    if ((scanf("%d%c", &n, &l) != 2 && l != '\n') || n > 4) {
        printf("n/a");
    } else {
        if (scanf("%d %d%c", &line, &column, &c) == 3 && c == '\n' && line <= NMAX && column <= NMAX) {
            switch (n) {
                case 1:
                    stati(line, column);
                    break;
                case 2:
                    dynamics1(line, column);
                    break;
                case 3:
                    dynamics2(line, column);
                    break;
                case 4:
                    dynamics3(line, column);
                    break;

                default:
                    printf("n/a");
                    break;
            }
        }
    }
    return 0;
}

void stati(int line, int column) {
    int arr[line][column], flag = 0;
    for (int i = 0; i < line; i++) {
        for (int j = 0; j < column; j++) {
            char c;
            if (scanf("%d%c", &arr[i][j], &c) != 2 && (c != '\n' || c != ' ')) flag = 1;
        }
    }
    if (flag != 0) {
        printf("N/A");
    } else {
        output(arr, line, column);
    }
}

void dynamics1(int line, int column) {
    int flag = 0;
    int **arr = malloc(line * column * sizeof(int *));
    for (int i = 0; i < line; i++) {
        arr[i] = malloc(column * sizeof(int));
    }
    for (int i = 0; i < line; i++) {
        for (int j = 0; j < column; j++) {
            char c;
            if (scanf("%d%c", &arr[i][j], &c) != 2 && (c != '\n' || c != ' ')) flag = 1;
        }
    }
    if (flag != 0) {
        printf("N/A");
    } else {
        output_d(arr, line, column);
    }
    for (int i = 0; i < line; i++) {
        free(arr[i]);
    }
    free(arr);
}

void dynamics2(int line, int column) {
    int flag = 0;
    int **arr = malloc(line * column * sizeof(int) + line * sizeof(int *));
    int *ptr = (int *)(arr + line);
    for (int i = 0; i < line; i++) {
        arr[i] = ptr + column * i;
    }
    for (int i = 0; i < line; i++) {
        for (int j = 0; j < column; j++) {
            char c;
            if (scanf("%d%c", &arr[i][j], &c) != 2 && (c != '\n' || c != ' ')) flag = 1;
        }
    }
    if (flag != 0) {
        printf("N/A");
    } else {
        output_d(arr, line, column);
    }
    free(arr);
}

void dynamics3(int line, int column) {
    int flag = 0;
    int **arr = malloc(line * sizeof(int *));
    int *values_array = malloc(line * column * sizeof(int));
    for (int i = 0; i < line; i++) {
        arr[i] = values_array + column * i;
    }
    for (int i = 0; i < line; i++) {
        for (int j = 0; j < column; j++) {
            char c;
            if (scanf("%d%c", &arr[i][j], &c) != 2 && (c != '\n' || c != ' ')) flag = 1;
        }
    }
    if (flag != 0) {
        printf("N/A");
    } else {
        output_d(arr, line, column);
    }
    free(values_array);
    free(arr);
}

void output(int arr[][NMAX], int line, int column) {
    for (int i = 0; i < line; i++) {
        for (int j = 0; j < column; j++) {
            if (j == column - 1) {
                printf("%d", arr[i][j]);
            } else {
                printf("%d ", arr[i][j]);
            }
        }
        printf("\n");
    }
}

void output_d(int **arr, int line, int column) {
    for (int i = 0; i < line; i++) {
        for (int j = 0; j < column; j++) {
            if (j == column - 1) {
                printf("%d", arr[i][j]);
            } else {
                printf("%d ", arr[i][j]);
            }
        }
        printf("\n");
    }
}