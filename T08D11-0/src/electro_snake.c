#include <stdio.h>
#include <stdlib.h>

/*
    1 6 7
    2 5 8
    3 4 9
*/
void sort_vertical(int **matrix, int line, int column);

/*
    1 2 3
    6 5 4
    7 8 9
*/
void sort_horizontal(int **matrix, int line, int column);
int input(int **matrix, int line, int column);
void output(int **matrix, int line, int column);

int main() {
    int line, column;
    char c;
    if (scanf("%d %d%c", &line, &column, &c) == 3 && c == '\n') {
        int **matrix = malloc(line * column * sizeof(int) + line * sizeof(int *));
        int *ptr = (int *)(matrix + line);
        for (int i = 0; i < line; i++) {
            matrix[i] = ptr + column * i;
        }
        if (input(matrix, line, column) != 1) {
            sort_vertical(matrix, line, column);
            sort_horizontal(matrix, line, column);
            free(matrix);
        } else {
            printf("N/A");
        }
    }
    return 0;
}

int input(int **matrix, int line, int column) {
    int flag = 0;
    for (int i = 0; i < line; i++) {
        for (int j = 0; j < column; j++) {
            char c;
            if (scanf("%d%c", &matrix[i][j], &c) != 2 && (c != '\n' || c != ' ')) flag = 1;
        }
    }
    return flag;
}

void output(int **matrix, int line, int column) {
    printf("\n");
    for (int i = 0; i < line; i++) {
        for (int j = 0; j < column; j++) {
            if (j == column - 1) {
                printf("%d", matrix[i][j]);
            } else {
                printf("%d ", matrix[i][j]);
            }
        }
        printf("\n");
    }
}

void sort_horizontal(int **matrix, int line, int column) {
    for (int k = 0; k < line; ++k) {
        for (int l = 0; l < column; ++l) {
            for (int i = 0; i < line; ++i) {
                for (int j = 0; j < column; ++j) {
                    if (i + 1 == line && j + 1 == column) {
                        continue;
                    } else {
                        if (j + 1 == column && matrix[i][j] > matrix[i + 1][0]) {
                            int t = matrix[i][j];
                            matrix[i][j] = matrix[i + 1][0];
                            matrix[i + 1][0] = t;
                        } else {
                            if (matrix[i][j] > matrix[i][j + 1]) {
                                int t = matrix[i][j];
                                matrix[i][j] = matrix[i][j + 1];
                                matrix[i][j + 1] = t;
                            }
                        }
                    }
                }
            }
        }
    }
    output(matrix, line, column);
}

void sort_vertical(int **matrix, int line, int column) {
    for (int k = 0; k < line; ++k) {
        for (int l = 0; l < column; ++l) {
            for (int i = 0; i < line; ++i) {
                for (int j = 0; j < column; ++j) {
                    if (i + 1 == line && j + 1 == column) {
                        continue;
                    } else {
                        if (j % 2 == 0) {
                            if (j + 1 == column && matrix[i][j] > matrix[i + 1][0]) {
                                int t = matrix[i][j];
                                matrix[i][j] = matrix[i + 1][0];
                                matrix[i + 1][0] = t;
                            } else {
                                if (matrix[i][j] > matrix[i][j + 1]) {
                                    int t = matrix[i][j];
                                    matrix[i][j] = matrix[i][j + 1];
                                    matrix[i][j + 1] = t;
                                }
                            }
                        } else if (i %2 != 0) {
                            
                        }
                    }
                }
            }
        }
        
    }
    output(matrix, line, column);
}