#include <math.h>
#include <stdio.h>

void fun();

int main() {
    fun();
    return 0;
}

void fun() {
    double x;
    char s;
    if (scanf("%lf%c", &x, &s) == 2 && s == '\n') {
        if (x != 0) {
            printf("%.1lf\n", (7e-3 * pow(x, 4) + ((22.8 * pow(x, (1 / 3)) - 1e3) * x + 3) / (x * x / 2) -
                               x * pow((10 + x), (2 / x)) - 1.01));
        } else {
            printf("Вы ввели 0, а просили не 0");
        }
    } else {
        printf("n/a");
    }
}