#include <stdio.h>

void circum(void);

int main() {
    circum();
    return 0;
}

void circum() {
    double x, y;
    char s;
    if (scanf("%lf %lf%c", &x, &y, &s) == 3 && s == '\n') {
        if ((x * x) + (y * y) < 25) {
            printf("GOTCHA");
        } else {
            printf("MISS");
        }
    }
}