#include <stdio.h>

int sum(int a, int b);

int main() {
    int num1;
    int num2;
    char str;
    printf("Введите целые числа\n");
    if (scanf("%d %d%c", &num1, &num2, &str) == 3 && str == '\n') {
        sum(num1, num2);
    } else {
        printf("n/a");
        return 1;
    }
    return 0;
}

int sum(int a, int b) {
    if (b != 0) {
        printf("%d %d %d %d\n", (a + b), (a - b), (a * b), (a / b));
    } else {
        printf("%d %d %d n/a\n", (a + b), (a - b), (a * b));
    }
    return 0;
}
