#include <stdio.h>
void max(void);

/* code */

int main() {
    max();
    return 0;
}

void max() {
    int num1, num2, c;
    char str;
    printf("Введите целые числа\n");
    if (scanf("%d %d%c", &num1, &num2, &str) == 3 && str == '\n') {
        c = (num1 > num2) ? num1 : num2;
        printf("%d\n", c);
    } else {
        printf("n/a\n");
    }
}